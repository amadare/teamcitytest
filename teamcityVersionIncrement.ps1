Param(
  [int]$releaseVersion,
  [string]$versionId
)
#Make all errors terminating errors to be able to do try-catch
$erroractionPreference="stop"

try {
	$releaseVersion++
	Write-Host "##teamcity[setParameter name='ProductVersion' value='$versionId.$releaseVersion']"
	#Write-Host "##teamcity[ProductVersion '$versionId.$releaseVersion']"
	#Write-Host "##teamcity[ReleaseVersion '$releaseVersion']"
}
catch {
	$ErrorMessage = $_.Exception.Message
	Write-Error "Setup version fail: $ErrorMessage" -ForegroundColor Red
    Exit 1	
}